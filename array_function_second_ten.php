 <!----------------array_search--------------->
<h2 style="text-align: center;color: red">array_search</h2>
<pre style="text-align: center">
    <?php
    $array = array(0 => 'blue', 1 => 'red', 2 => 'green', 3 => 'red');

    echo $key = array_search('green', $array);
    echo '</br>';
    echo $key = array_search('red', $array);
    ?>
</pre>




 <!----------------array_reverse--------------->


<h2 style="text-align: center;color: red">array_reverse</h2>
<pre style="text-align: center">
    <?php
    $input = array("php", 4.0, array("green", "red"));
    $reversed = array_reverse($input);
    print_r($input);
    ?>
</pre>






 <!----------------array_shift--------------->

<h2 style="text-align: center;color: red">array_shift</h2>
<pre style="text-align: center">
    <?php
    $stack = array("orange", "banana", "apple", "raspberry");
    $fruit = array_shift($stack);
    print_r($stack);
    ?>
</pre>





 <!----------------array_sum--------------->

<h2 style="text-align: center;color: red">array_sum</h2>
<pre style="text-align: center">
    <?php
    $a = array(2, 4, 6, 8);
    echo "sum(a) = " . array_sum($a) . "\n";
    ?>
</pre>





 <!----------------array_unique--------------->


<h2 style="text-align: center;color: red">array_unique</h2>
<pre style="text-align: center">
    <?php
    $input = array("a" => "green", "red", "b" => "green", "blue", "red");
    $result = array_unique($input);
    print_r($result);
    ?>
</pre>






 <!----------------array_unshift--------------->

<h2 style="text-align: center;color: red">array_unshift</h2>
<pre style="text-align: center">
    <?php
    $queue = array("orange", "banana");
    array_unshift($queue, "apple", "raspberry");
    print_r($queue);
    ?>
</pre>





 <!----------------array_values--------------->

<h2 style="text-align: center;color: red">array_values</h2>
<pre style="text-align: center">
    <?php
    $array = array("size" => "XL", "color" => "gold");
    print_r(array_values($array));
    ?>
</pre>






 <!----------------array_walk--------------->

<h2 style="text-align: center;color: red">array_walk</h2>
<pre style="text-align: center">
    <?php
    $fruits = array("d" => "lemon", "a" => "orange", "b" => "banana", "c" => "apple");

    function test_alter(&$item1, $key, $prefix) {
        $item1 = "$prefix: $item1";
    }

    function test_print($item2, $key) {
        echo "$key. $item2<br />\n";
    }

    echo "Before ...:\n";
    array_walk($fruits, 'test_print');

    array_walk($fruits, 'test_alter', 'fruit');
    echo "... and after:\n";

    array_walk($fruits, 'test_print');
    ?>
</pre>






 <!----------------asort--------------->


<h2 style="text-align: center;color: red">asort</h2>
<pre style="text-align: center">
    <?php
    $fruits = array("d" => "lemon", "a" => "orange", "b" => "banana", "c" => "apple");
    asort($fruits);
    foreach ($fruits as $key => $val) {
        echo "$key = $val\n";
    }
    ?>
</pre>






 <!----------------compact--------------->

<h2 style="text-align: center;color: red">compact</h2>
<pre style="text-align: center">
    <?php
    $city = "San Francisco";
    $state = "CA";
    $event = "SIGGRAPH";

    $location_vars = array("city", "state");

    $result = compact("event", "nothing_here", $location_vars);
    print_r($result);
    ?>
</pre>
