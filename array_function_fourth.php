<!----------------reset--------------->
<h2 style="text-align: center;color: red">reset</h2>
<pre style="text-align: center">
    <?php
    $transport = array('foot', 'bike', 'car', 'plane');
    echo $mode = current($transport);

    echo '</br>';
    echo $mode = next($transport);
    echo '</br>';
    echo $mode = prev($transport);
    echo '</br>';
    echo $mode = end($transport);
    echo '</br>';
    reset($transport);
    echo $mode = current($transport);
    ?>
</pre>





<!----------------shuffle--------------->

<h2 style="text-align: center;color: red">shuffle</h2>
<pre style="text-align: center">
    <?php
    $numbers = range(1, 20);
    shuffle($numbers);
    foreach ($numbers as $number) {
        echo "$number ";
    }
    ?>
</pre>





<!----------------sizeof--------------->

<h2 style="text-align: center;color: red">sizeof</h2>
<pre style="text-align: center">
    <?php
    $a[0] = 1;
    $a[1] = 3;
    $a[2] = 5;
    echo $result = sizeof($a);
    ?>
</pre>





<!----------------sort--------------->

<h2 style="text-align: center;color: red">sort</h2>
<pre style="text-align: center">
    <?php
    $fruits = array("lemon", "orange", "banana", "apple");
    sort($fruits);
    foreach ($fruits as $key => $val) {
        echo "fruits[" . $key . "] = " . $val . "\n";
    }
    ?>
</pre>


