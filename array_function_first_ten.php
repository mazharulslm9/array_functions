<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <!----------------array_combine--------------->
        
        <h2 style="text-align: center;color: red">array_combine</h2>
        <pre style="text-align: center">
            <?php
            $a = array('green', 'red', 'yellow');
            $b = array('avocado', 'apple', 'banana');
            $c = array_combine($a, $b);

            print_r($c);
            ?>
        </pre>
        
        
        
        <!----------------array_filter--------------->
        
        <h2 style="text-align: center;color: red">array_filter</h2>
        <pre style="text-align: center">
            <?php

            function odd($var) {
                // returns whether the input integer is odd
                return($var & 1);
            }

            function even($var) {
                // returns whether the input integer is even
                return(!($var & 1));
            }

            $array1 = array("a" => 1, "b" => 2, "c" => 3, "d" => 4, "e" => 5);
            $array2 = array(6, 7, 8, 9, 10, 11, 12);

            echo "Odd :\n";
            print_r(array_filter($array1, "odd"));
            echo "Even:\n";
            print_r(array_filter($array2, "even"));
            ?>
        </pre>
        
        
        
        
         <!----------------array_key_exists--------------->
        
        <h2 style="text-align: center;color: red">array_key_exists</h2>
        <pre style="text-align: center">
            <?php
            $search_array = array('first' => 1, 'second' => 4);
            if (array_key_exists('third', $search_array)) {
                echo "The 'first' element is in the array";
            } else {
                echo 'The key you are looking for is not in the array.';
            }
            ?>
        </pre>
        
        
        
        
        
           <!----------------array_key--------------->
        
        <h2 style="text-align: center;color: red">array_key</h2>
        <pre style="text-align: center">
            <?php
            $array = array(0 => 100, "color" => "red", 8 => "blue");
            print_r(array_keys($array));

            $array = array("blue", "red", "green", "blue", "blue", "green");
            print_r(array_keys($array, "green"));

            $array = array("color" => array("blue", "red", "green"),
                "size" => array("small", "medium", "large"));
            print_r(array_keys($array));
            ?>
        </pre>
        
        
        
        
        
        <!----------------array_pad--------------->
        
        
        <h2 style="text-align: center;color: red">array_pad</h2>
        <pre style="text-align: center">
            <?php
            $input = array(12, 10, 9);

            print_r($result = array_pad($input, 5, 0));
            // result is array(12, 10, 9, 0, 0)

            print_r($result = array_pad($input, -7, -1));
            // result is array(-1, -1, -1, -1, 12, 10, 9)
            ?>
        </pre>
        
        
        
        
        
        <!----------------array_pop--------------->
        
        <h2 style="text-align: center;color: red">array_pop</h2>
        <pre style="text-align: center">
            <?php
            $stack = array("orange", "banana", "apple", "raspberry", "jack fruit", "mango");
            $fruit = array_pop($stack);
            print_r($stack);
            ?>
        </pre>
        
        
        
        
        
         <!----------------array_push--------------->
        
        
        <h2 style="text-align: center;color: red">array_push</h2>
        <pre style="text-align: center">
            <?php
            $stack = array("orange", "banana", "apple", "raspberry");
            $fruit = array_push($stack, "jack fruit", "mango");
            print_r($stack);
            ?>
        </pre>
        
        
        
        
        
         <!----------------array_replace--------------->
        
        <h2 style="text-align: center;color: red">array_replace</h2>
        <pre style="text-align: center">
            <?php
            $base = array("orange", "banana", "apple", "raspberry");
            $replacements = array(0 => "pineapple", 4 => "cherry");
            $replacements2 = array(0 => "grape");

            $basket = array_replace($base, $replacements, $replacements2);
            print_r($basket);
            ?>
        </pre>
        
        
        
        
         <!----------------array_rand--------------->
        
        <h2 style="text-align: center;color: red">array_rand</h2>
        <pre style="text-align: center">
            <?php
            $input = array("Neo", "Morpheus", "Trinity", "Cypher", "Tank");
            $rand_keys = array_rand($input, 2);
            echo $input[$rand_keys[0]] . "\n";
            echo $input[$rand_keys[1]] . "\n";
            ?>
        </pre>
        
        
        
        
        
        <!----------------array_replace_recursive--------------->
        
        <h2 style="text-align: center;color: red">array_replace_recursive</h2>
        <pre style="text-align: center">
            <?php
            $base = array('citrus' => array("orange"), 'berries' => array("blackberry", "raspberry"),);
            $replacements = array('citrus' => array('pineapple'), 'berries' => array('blueberry'));

            $basket = array_replace_recursive($base, $replacements);
            print_r($basket);

            $basket = array_replace($base, $replacements);
            print_r($basket)
            ?>
        </pre>
    </body>
</html>
