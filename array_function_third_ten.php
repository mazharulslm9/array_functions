 <!----------------count--------------->
<h2 style="text-align: center;color: red">count</h2>
<pre style="text-align: center">
    <?php
    $a[0] = 1;
    $a[1] = 3;
    $a[2] = 5;
    echo $result = count($a);
    ?>
</pre>




 <!----------------each--------------->

<h2 style="text-align: center;color: red">each</h2>
<pre style="text-align: center">
    <?php
    $foo = array("fred", "jussi", "jouni", "egon", "marliese");
    $bar = each($foo);
    print_r($bar);
    ?>
</pre>





 <!----------------end--------------->

<h2 style="text-align: center;color: red">end</h2>
<pre style="text-align: center">
    <?php
    $fruits = array('apple', 'banana', 'cranberry');
    echo end($fruits);
    ?>
</pre>





<!----------------extract--------------->

<h2 style="text-align: center;color: red">extract</h2>
<pre style="text-align: center">
    <?php
    $size = "large";
    $var_array = array("color" => "blue",
        "size" => "medium",
        "shape" => "sphere");
    extract($var_array, EXTR_PREFIX_SAME, "wddx");

    echo "$color, $size, $shape, $wddx_size\n";
    ?>
</pre>





<!----------------in_array--------------->

<h2 style="text-align: center;color: red">in_array</h2>
<pre style="text-align: center">
    <?php
    $os = array("Mac", "NT", "Irix", "Linux");
    if (in_array("Irix", $os)) {
        echo "Got Irix";
    }
    if (in_array("mac", $os)) {
        echo "Got mac";
    }
    ?>
</pre>






<!----------------key--------------->

<h2 style="text-align: center;color: red">key</h2>
<pre style="text-align: center">
    <?php
    $array = array(
        'fruit1' => 'apple',
        'fruit2' => 'orange',
        'fruit3' => 'grape',
        'fruit4' => 'apple',
        'fruit5' => 'apple');


    while ($fruit_name = current($array)) {
        if ($fruit_name == 'apple') {
            echo key($array) . '<br />';
        }
        next($array);
    }
    ?>
</pre>





<!----------------list--------------->

<h2 style="text-align: center;color: red">list</h2>
<pre style="text-align: center">
    <?php
    $info = array('coffee', 'brown', 'caffeine');

// Listing all the variables
    list($drink, $color, $power) = $info;
    echo "$drink is $color and $power makes it special.\n";
    ?>
</pre>






<!----------------next--------------->

<h2 style="text-align: center;color: red">next</h2>
<pre style="text-align: center">
    <?php
    $transport = array('foot', 'bike', 'car', 'plane');
    echo $mode = current($transport);
    echo '</br>';
    echo $mode = next($transport);
    echo '</br>';
    echo $mode = next($transport);
    echo '</br>';
    echo $mode = prev($transport);
    echo '</br>';
    echo $mode = end($transport);
    ?>
</pre>







<!----------------pos--------------->

<h2 style="text-align: center;color: red">pos</h2>
<pre style="text-align: center">
    <?php
    $transport = array('foot', 'bike', 'car', 'plane');
    echo $mode = pos($transport);
    echo '</br>';
    echo $mode = next($transport);
    echo '</br>';
    echo $mode = pos($transport);
    ?>
</pre>






<!----------------prev--------------->

<h2 style="text-align: center;color: red">prev</h2>
<pre style="text-align: center">
    <?php
    $transport = array('foot', 'bike', 'car', 'plane');
    echo $mode = current($transport);
    
    echo '</br>';
    echo $mode = next($transport);
    echo '</br>';
    echo $mode = prev($transport);
    echo '</br>';
    echo $mode = end($transport);
    ?>
</pre>
